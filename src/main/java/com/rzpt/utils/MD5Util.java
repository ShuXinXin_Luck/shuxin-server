package com.rzpt.utils;

import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component
public class MD5Util {

//    md5加密
    public static String md5(String context) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(context.getBytes());
        String hashedPwd = new BigInteger(1, md.digest()).toString(16);// 16是表示转换为16进制数
        return hashedPwd;
    }
}
