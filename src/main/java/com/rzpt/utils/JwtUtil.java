package com.rzpt.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

@Component
@Slf4j
public class JwtUtil {

    public static String JWTKEY;

    public static String genToken(Map<String, Object> claims) {
        log.info(JWTKEY);
        return
                JWT.create()
                        .withClaim("claims", claims)
//                        7天token过期
                        .withExpiresAt(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 7))
                        .sign(Algorithm.HMAC256(JWTKEY));
    }

    public static Map<String, Object> paseJWT(String token) {
        return
                JWT.require(Algorithm.HMAC256(JWTKEY))
                        .build()
                        .verify(token)
                        .getClaim("claims")
                        .asMap();
    }

    @Value("${jwtPro.JWTKEY}")
    public void setJWTKEY(String JWTKEY) {
        JwtUtil.JWTKEY = JWTKEY;
    }
}
