package com.rzpt.serverice.impl;

import com.rzpt.mapper.UserMapper;
import com.rzpt.pojo.impl.User;
import com.rzpt.serverice.UserService;
import com.rzpt.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    //    查询账号
    @Override
    public User findByAccount(String account) {
        User u = userMapper.findByAccount(account);
        return u;
    }

    //
    @Override
    public void register(String account, String password) {
        userMapper.add(account, password);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateUser(user);
    }

    @Override
    public void updateHeaderImg(String headerUrl) {
        Map<String, Object> map = ThreadLocalUtil.get();
        String account = (String) map.get("account");
        userMapper.updateHeaderUrl(headerUrl, account);
    }

    @Override
    public void updatePwd(String pwd,String id) {
        userMapper.updatePwd(pwd,id);
    }

}
