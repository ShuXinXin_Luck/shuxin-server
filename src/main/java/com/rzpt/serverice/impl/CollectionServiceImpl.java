package com.rzpt.serverice.impl;

import com.rzpt.mapper.CollectionMapper;
import com.rzpt.pojo.impl.Collection;
import com.rzpt.serverice.CollectionService;
import com.rzpt.utils.ThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class CollectionServiceImpl implements CollectionService {
    @Autowired
    CollectionMapper collectionMapper;

    @Override
    public List<Collection> list() {
        Map<String, Object> map = ThreadLocalUtil.get();
        List<Collection> rs = collectionMapper.list((String) map.get("account"));

        return rs;
    }

    @Override
    public void add(Collection collection) {
        collection.setCategoryTime(LocalDateTime.now());
        log.info("shijian", LocalDateTime.now());
        Map<String, Object> map = ThreadLocalUtil.get();
        String userAccount = (String) map.get("account");
        collection.setCategoryUser(userAccount);
        collectionMapper.add(collection);
    }
}
