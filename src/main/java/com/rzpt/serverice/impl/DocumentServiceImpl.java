package com.rzpt.serverice.impl;

import com.rzpt.mapper.DocumentMapper;
import com.rzpt.pojo.impl.Doc;
import com.rzpt.serverice.DocumentService;
import com.rzpt.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    DocumentMapper documentMapper;

    @Override
    public void add(Doc doc) {

        Map<String, Object> map = ThreadLocalUtil.get();
        doc.setUser((String) map.get("account"));
        documentMapper.add(doc);
    }
}
