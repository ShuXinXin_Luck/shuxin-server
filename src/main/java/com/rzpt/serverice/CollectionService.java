package com.rzpt.serverice;


import com.rzpt.pojo.impl.Collection;


import java.util.List;

public interface CollectionService {
    List<Collection> list();

    void add(Collection collection);
}
