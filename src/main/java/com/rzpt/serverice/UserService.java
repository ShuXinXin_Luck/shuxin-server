package com.rzpt.serverice;

import com.rzpt.pojo.impl.User;

public interface UserService {


    User findByAccount(String id);

    void register(String account, String password);

    void updateUser(User user);

    void updateHeaderImg(String headerUrl);

    void updatePwd(String pwd , String account);
}
