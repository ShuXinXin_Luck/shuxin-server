package com.rzpt.controller;

import com.rzpt.pojo.impl.Doc;
import com.rzpt.pojo.impl.Result;
import com.rzpt.serverice.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/doc")
public class DocumentController {
    @Autowired
    private DocumentService documentService;

    @GetMapping("/add")
    public Result add(@RequestBody Doc doc) {

        documentService.add(doc);
        return Result.success();

    }
}
