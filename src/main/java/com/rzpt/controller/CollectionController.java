package com.rzpt.controller;

import com.rzpt.pojo.impl.Collection;
import com.rzpt.pojo.impl.Result;
import com.rzpt.serverice.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/collection")
public class CollectionController {
    @Autowired
    private CollectionService CollectionService;

    /*
     * 添加收藏分类
     *
     */
    @PostMapping
    public Result add(@RequestBody Collection collection) {
        CollectionService.add(collection);
        return Result.success();
    }

    /*
     * 获取收藏分类
     *
     */
    @GetMapping
    public Result<List<Collection>> list(){
        List<Collection> rs = CollectionService.list();
        return Result.success(rs);
    }
}
