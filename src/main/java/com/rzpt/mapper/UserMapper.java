package com.rzpt.mapper;

import com.rzpt.pojo.impl.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface UserMapper {

    @Insert("insert into userinfo(account,password,date)" +
            " values (#{account},#{password},now())")
    void add(String account, String password);

    @Select("select * from userinfo where account =#{account}")
    User findByAccount(String account);

    @Update("update userinfo set name=#{name} where account=#{account}")
    void updateUser(User user);

    @Update("update userinfo set header_img=#{headerUrl} where account=#{account}")
    void updateHeaderUrl(String headerUrl, String account);

    @Update("update userinfo set password=#{password} where account = #{account}")
    void updatePwd(String password,String account);
}
