package com.rzpt.mapper;

import com.rzpt.pojo.impl.Collection;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CollectionMapper {
    @Select("SELECT * from category WHERE categoryUser =#{account}")
    List<Collection> list(String account);

    @Insert("insert into category(categoryName,categoryUser,categoryTime) " +
            "VALUES(#{categoryName},#{categoryUser},#{categoryTime}) ")
    void add(Collection collection);
}
