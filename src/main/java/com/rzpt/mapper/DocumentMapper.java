package com.rzpt.mapper;


import com.rzpt.pojo.impl.Doc;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DocumentMapper {

    @Insert("INSERT INTO doc(title,context,cover_img,category_id,user,create_time,update_time) " +
            "Values (#{title},#{context},#{cover_img},#{category_id},#{user},now(),now())")
    void add(Doc doc);
}
