package com.rzpt.pojo.impl;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

import java.time.LocalDateTime;


@Data
public class Collection {
    private Integer id;
//    @NotEmpty
    private String categoryName;
//    @NotEmpty
    private String categoryUser;
//    @JsonFormat(pattern = "yyyy-MM-ddd HH:mm:ss")
    private LocalDateTime categoryTime;


}
