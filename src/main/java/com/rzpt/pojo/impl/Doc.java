package com.rzpt.pojo.impl;

import lombok.Data;

import java.util.Date;

@Data
public class Doc {
    private Integer id;
    private String title;
    private String context;
    private String cover_img;
    private String category_id;
    private String user;
    private Date create_time;
    private Date update_time;
}
