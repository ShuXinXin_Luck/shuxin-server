package com.rzpt.pojo.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.Pattern;
import lombok.Data;


import java.util.Date;

@Data
public class User {
    @JsonIgnore
    private Integer id;
    private String account;
    @JsonIgnore
    private String password;
    private String header_img;
    private Integer vip;
    private Date date;
    @Pattern(regexp = "^\\S{2,8}$")
    private String name;
}
